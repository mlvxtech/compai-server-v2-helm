## Installing prerequisites

#### Essentials

Run the following command to install essentialls such as [git](https://git-scm.com/) and [curl](https://curl.haxx.se/).

```
sudo apt install git apt-transport-https ca-certificates curl gnupg2 software-properties-common

```

#### Docker

Kubernetes requries Docker to run. Follow the instructions [here](https://docs.docker.com/install/linux/docker-ce/debian/) to install.

#### Kubernetes

* If deploying on the cloud, refer to your cloud provider's documentation on how to deploy a [Kubernetes](http://kubernetes.io) cluster.
Instructions for deploying on Google Cloud Platform, for instance, can be found [here](https://cloud.google.com/kubernetes-engine/docs/how-to/creating-a-cluster).

* If deploying on single machine, follow the instructions [here](https://kubernetes.io/docs/tasks/tools/install-minikube/) to install Minikube.

#### Kubectl

Once Kubernetes is installed, make sure `kubectl` is installed by following the instruction [here](https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-linux).

#### Helm

[Helm](https://helm.sh) is Kubernetes' package manager. Follow the instructions [here](https://helm.sh/docs/intro/install/) for installing on your kubernetes cluster.

## Setup

#### Minikube

If Minkube was installed, start a cluster with the following command. If a Kubernetes cluster was installed on the cloud instead, skip this step.

```
sudo minikube start --vm-driver none --disk-size 80GB
```

If only one CPU is available on the host machine, you will need to append extra parameters as shown below:

```
sudo minikube start --vm-driver none --disk-size 80GB --force --cpus 1 --extra-config=kubeadm.ignore-preflight-errors=NumCPU
```

**Note**: Consider adding the above `minikube` command to a startup script (e.g. cron) to ensure that the Minikube cluster gets started at every reboot of the machine or VM.

#### Cluster setup

1. Configure your login credentials to the container registry by running the following command:

    ```
    kubectl create secret docker-registry regcred --docker-server=https://harbor.compression.ai/ --docker-username=<your-name> --docker-password=<your-pword>
    ```

2. Install the Compression.ai Helm chart by navigating to the root of this repository and running the following:

    ```
    helm install my-release .
    ```

where `my-release` is the desired name of the installation. Navigate [here](https://bitbucket.org/mlvxtech/compai-server-v2-helm/src/master/README.md) for more information on how to customize the installation.


**Note**: If secret name (`regcred`) is changed in the previous step, be sure to update the `imagePullSecrets` value when the chart is deployed.

## Usage

The Compression.ai server is now available on the `celeryServer.service.nodePort` (port `31077` by default).

A compression request is submitted following the format below:

```
curl http://104.155.152.203:31077/compress_url -X POST --header 'Content-Type: application/json' \
--data '{"image_url":"file://2K.jpg", "user_id" : "tester", "out_format": "jpg", "quality": 80}'
```

**Reminder**: The `image_url` field as well as the output directory are relative to the `celeryServer.localInputPath` and `celeryServer.localOutputPath` parameters respectively of the helm [chart](https://bitbucket.org/mlvxtech/compai-server-v2-helm/src/master/README.md).

A convenience endpoint also exists to compress an entire folder at once:

```
curl http://104.155.152.203:31077/compress_local_dir -X POST --header 'Content-Type: application/json' \
--data '{"dir_uri": "file://", "user_id": "testing", "out_format": "jpg", "quality": 80}'
```

Note that callbacks aren't enabled for this method, since individual asset ids must be attached to each image in order to sort the callbacks.

Use the `compress_url_batch` endpoint instead if callbacks are required:

```
curl http://104.155.152.203:31077/compress_url_batch -X POST --header 'Content-Type: application/json' --data \   
 '[{
    "image_url": "file://1/veeterzy-82537.png",  
    "user_id": "testing",  
    "out_format": "jpg",
    "unique_asset_id":1,
  	"callback_url":"https://compai.free.beeceptor.com"
  },  
  {  
    "image_url": "file://2/2016_audi_r8_4k-HD.jpg",   
    "user_id": "testing",  
    "out_format": "jpg",
    "unique_asset_id":2,
  	"callback_url":"https://compai.free.beeceptor.com"
  }  
]'
```
