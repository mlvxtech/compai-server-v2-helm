# Compression.ai Compression Server

[Compression.ai](https://compression.ai/) uses machine learning to compress data.

## Introduction

This chart bootstraps a compression server on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

## Prerequisites

- Kubernetes 1.10+ with Beta APIs enabled
- PV provisioner support in the underlying infrastructure

## Installing the Chart

To install the chart with the release name `my-release`:

```bash
$ helm install my-release .
```

The command deploys the Compression.ai server on the Kubernetes cluster in the default configuration. The [configuration](#configuration) section lists the parameters that can be configured during installation.


**Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```bash
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release completely.

## Configuration

The following table lists the configurable parameters of the MySQL chart and their default values.

| Parameter                                    | Description                                                                                  | Default                                              |
| -------------------------------------------- | -------------------------------------------------------------------------------------------- | ---------------------------------------------------- |
| `imagePullSecrets`                           | Name of Secret resource containing private registry credentials                              | `regcred`                                                 |
| `celeryServer.name`                          | subname of the Celery pod. Actual pod name will be {ReleaseName}-{subname}                   | `celery`                                             |
| `celeryServer.replicaCount`                  | The number of Celery pods to spawn                                                           | `1`                                                  |
| `celeryServer.progressDeadlineSeconds`       | Number of seconds to wait before setting the Celery deployment status to "failed"            | `600`                                                |
| `celeryServer.revisionHistoryLimit`          | Number of old Celery ReplicaSets to retain to allow rollback                                 | `10`                                                 |
| `celeryServer.maxSurge`                      | Maximum number of replicas to spawn during a rolling update of Celery                        | `1`                                                  |
| `celeryServer.maxUnavailable`                | Maximum number of unavailable pods during a rolling update of Celery                         | `0`                                                  |
| `celeryServer.nodeSelector`                  | Node labels for Celery pod assignment                                                        | {}                                                   |
| `celeryServer.image.repository`              | Celery image repository                                                                      | `harbor.compression.ai/terramera/diff-jpeg-celery-server` |
| `celeryServer.image.tag`                     | Celery image tag                                                                             | `1.1.1`                                              |
| `celeryServer.image.pullPolicy`              | Celery image pull policy                                                                     | `Always`                                             |
| `celeryServer.resources`                     | Celery resource requests/limits                                                              | {}                                                   |
| `celeryServer.terminationGracePeriodSeconds` | Time to wait before forecefully shutting down the Celery pods                                | `120`                                                |
| `celeryServer.localInputPath`                | The root path for all compression requests starting with a `file://` uri                     | `/tmp/local-input`                                   |
| `celeryServer.localOutputPath`               | The root path used to output the result of compression requests started with a `file://` uri | `/tmp/local-output`                                  |
| `celeryServer.containerPort`                 | The listening pods for the Celery pods                                                       | `8000`                                               |
| `celeryServer.service.name`                  | The name of the service used to communicate with the Celery pods                             | `celery-service`                                     |
| `celeryServer.service.nodePort`              | The port used to communicate with the Celery (API) service                                   | `31077`                                              |
| `celeryServer.service.type`                  | The type of service to use to communicate with the Celery pods                               | `NodePort`                                           |
| `celeryServer.configmap.name`                | Name of the configmap to use fdor the Celery pods                                            | `celery-configmap`                                   |
| `celeryServer.configmap.celeryConcurrency`   | The number of Celery workers                                                                 | `8`                                                  |
| `celeryServer.configmap.logLevel`            | The log level for the Celery pods                                                            | `error`                                              |
| `modelServer.name`                           | subname of the model server pod. Actual pod name will be {ReleaseName}-{subname}             | `model-server`                                             |
| `modelServer.replicaCount`                   | The number of model server pods to spawn                                                     | `1`                                                  |
| `modelServer.progressDeadlineSeconds`        | Number of seconds to wait before setting the model server deployment status to "failed"      | `600`                                                |
| `modelServer.revisionHistoryLimit`           | Number of old model server ReplicaSets to retain to allow rollback                           | `10`                                                 |
| `modelServer.maxSurge`                       | Maximum number of replicas to spawn during a rolling update of the model server              | `1`                                                  |
| `modelServer.maxUnavailable`                 | Maximum number of unavailable pods during a rolling update of the model server               | `0`                                                  |
| `modelServer.deploymentStrategy`             | The strategy used when updating the model server deployment                                  | `RollingUpdate`                                      |
| `modelServer.nodeSelector`                   | Node labels for model server pod assignment                                                  | {}                                                   |
| `modelServer.image.repository`               | Model server image repository                                                                | `harbor.compression.ai/terramera/diff-jpeg-model-server` |
| `modelServer.image.tag`                      | Model server image tag                                                                       | `2.0.0`                                              |
| `modelServer.image.pullPolicy`               | Model server image pull policy                                                               | `IfNotPresent`                                       |
| `modelServer.resources`                      | Model server resource requests/limits                                                        | {}                                                   |
| `modelServer.containerPort`                  | Port on the Celery deployment used to communicate with the model server                      | `9000`                                               |
| `modelServer.terminationGracePeriodSeconds`  | Time to wait before forecefully shutting down the model server pods                          | `120`                                                |
| `modelServer.service.name`                   | The name of the service used to communicate with the model server pods                       | `model-server-service`                               |
| `modelServer.service.type`                   | The type of service to use to communicate with the model server pods                         | `ClusterIP`                                          |
| `rabbitmq.name`                              | subname of the rabbitmq pod. Actual pod name will be {ReleaseName}-{subname}                 | `queue`                                              |
| `rabbitmq.image.repository`                  | Rabbitmq image repository                                                                    | `harbor.compression.ai/terramera/mlvx-rabbitmq`      |
| `rabbitmq.image.tag`                         | Rabbitmq server image tag                                                                    | `2.0.1`                                              |
| `rabbitmq.image.pullPolicy`                  | Rabbitmq server image pull policy                                                            | `Always`                                             |
| `rabbitmq.storageClass.name`                 | Type of persistent volume claim                                                              | `rabbitmq-storage-class`                             |
| `rabbitmq.persistentVolume.name`             | Name of the persistent volume used                                                           | `rabbitmq-storage`                                   |
| `rabbitmq.persistentVolume.capacity`         | Capacity of the persistent volume to use                                                     | `20Gi`                                               |
| `rabbitmq.persistentVolume.hostPath`         | Path of the persistent volume on the host machine                                            | `/tmp/rabbitmq`                                      |
| `rabbitmq.persistentVolumeClaim.name`        | Name of the persistent volume claim to use                                                   | `rabbitmq-pvc`                                       |
| `rabbitmq.configmap.name`                    | Name of the Rabbitmq configmap                                                               | `rabbitmq-configmap`                                 |


Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`. For example,

```bash
$ helm install my-release \
  --set celeryServer.configmap.logLevel=info,celeryServer.service.nodePort=31066 \
    .
```

The above command sets log level to `info` and the API port to `31066`.

Alternatively, a YAML file that specifies the values for the parameters can be provided while installing the chart. For example,

```bash
$ helm install my-release -f values.yaml .
```

**Tip**: You can use the default [values.yaml](values.yaml)