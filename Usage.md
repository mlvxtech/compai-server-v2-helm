## Installing prerequisites

### Essentials

Run the following command to install essentialls such as [git](https://git-scm.com/) and [curl](https://curl.haxx.se/).

```
sudo apt install git apt-transport-https ca-certificates curl gnupg2 software-properties-common

```

### Docker

Kubernetes requries Docker to run. Follow the instructions [here](https://docs.docker.com/install/linux/docker-ce/debian/) to install.

### Kubernetes

If deploying on the cloud, refer to your cloud provider's documentation on how to deploy a [Kubernetes](http://kubernetes.io) cluster.

Instructions for deploying on Google Cloud Platform can be found [here](https://cloud.google.com/kubernetes-engine/docs/how-to/creating-a-cluster).

If deploying on single machine, follow the instructions bellow for installing Minikube.

#### Minikube

Complete install instructions can be found [here](https://kubernetes.io/docs/tasks/tools/install-minikube/).

After install Minikube, start a Minikube cluster with the following command:

```
sudo minikube start --vm-driver none --disk-size 80GB
```

Note that if only one CPU is available on the host machine, you will need to append extra parameters as shown below:

```
sudo minikube start --vm-driver none --disk-size 80GB --force --cpus 1 --extra-config=kubeadm.ignore-preflight-errors=NumCPU
```

### Kubectl

Once Kubernetes is installed, make sure `kubectl` is installed by following the instruction [here](https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-linux).

### Helm

[Helm](https://helm.sh) is Kubernetes' package manager. Follow the instructions [here](https://helm.sh/docs/intro/install/) for installing on your kubernetes cluster.

## Setup

- Configure your login credentials to the container registry by running the following command:


```
kubectl create secret docker-registry regcred --docker-server=https://harbor.compression.ai/ --docker-username=<your-name> --docker-password=<your-pword>
```

- Install the Compression.ai Helm chart as described [here](https://bitbucket.org/mlvxtech/compai-server-v2-helm/src/master/README.md).

> **Note**: If you change the secret name (`regcred`) in the previous step, be sure to update the `imagePullSecrets` value when you deploy the chart

## Usage

Submit a compression request using the following format. 

> **Reminder**: The `image_url` field as well as the output directory are relative to the `celeryServer.localInputPath` and `celeryServer.localOutputPath` parameters respectively of the helm [chart](https://bitbucket.org/mlvxtech/compai-server-v2-helm/src/master/Readme.md).



```
curl http://104.155.152.203:31077/compress_url -X POST --header 'Content-Type: application/json' \
--data '{"image_url":"file://2K.jpg", "unique_asset_id" : 12345, "user_id" : "tester", "out_format": "jpg", "quality": 80}'
```


